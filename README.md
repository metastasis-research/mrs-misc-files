# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact

Release 7/14/2017
Metastasis Research Society Bug and Enhancement Release


#5262017-1011 Enhancement - Changes to the rules which regulate the automated email responses. Please see associated word document Automated Website Emails 5-2017-1

#5262017-1012 Bug - The link to the journal Angiogenesis under the Scientist/Clinician tab doesn't seem to be working due to a security issue (the s in https). https://www.metastasis-research.com/angiogenesis should be https://www.metastasis-research.org/angiogenesis changed in main menu url call.

#6282017-1013 Modification - Donation slider: 
Our vision is to de-throne metastasis as a disease that steals lives by obtaining research results that translate to patients having an optimal quality of life while managing metastasis as a chronic condition, with the ultimate goal of achieving a cure. (an instead of a)

Membership slider:
The Benefits of Membership: Find out why you should join the MRS. (delete period and remove capitalization of first part)
Changed to: The benefits of membership: find out why you should join the MRS

Upcoming conferences slider:
The MRS organizes an international congress every two years to provide a venue dedicated to the exchange of information and furtherance of research into all aspects of metastasis.
(an instead of a and deleted the d from provided)

#6282017-1014 Enhancement - Current sentence on registration page:
The MRS invites Supporting Members to take a “Seat at the Bench” and lend their voices to the development of patient-centered metastasis research projects/events and/or grants. Supporting Members can choose to participate in one or both of our member directories:

Please change this to add a bit more:
The MRS invites Supporting Members to take a “Seat at the Bench” and lend their voices to the development of patient-centered metastasis research projects/events and/or grants. Supporting Members can choose to participate in one or both of our member directories listed below.  
(Note: Directory participation does not require payment for membership on the next screen to enable those facing financial hardship to still participate actively in progressing patient-centered metastasis research.  However, only paid members will receive Supporting Member benefits).


#6282017-1015 Bug - v_front_page_news changed to plain text format to match the rest of the blocks on front page.

#6282017-1016 Enhancement - Try to use rules to modify email messages when members role is expiring and members role has expired. Unfortunately, the rules were not mutally exclusive and resulted in both members role and all other roles were expiring or had expired. Two messages were sent out, so modified rules messages to reflect issue of old members role expiring or had expired. Placed a notification indicating to disregard the message if the role had either expired or was about to expire.

#6282017-1017 Enhancement - Journal access per Springer's request/instructions and modifications to allow MRS members to access the journals.